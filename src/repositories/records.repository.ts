import {
  Records_Model,
  OutputModel,
  InputModel,
  QueryModel,
} from "./../models";
import { HttpStatusCode } from "@soham/config";
import { CustomError } from "../error/customError";
import { BaseRepository } from "./base.repository";
export class RecordRepo extends BaseRepository {
  public getKeyCounts(inputData: InputModel): Promise<OutputModel[]> {
    return new Promise(async (resolve, reject) => {
      try {
        const queryModel = new QueryModel(inputData);
        switch (this.envConfigs.DB.CONNECTION.toUpperCase()) {
          case "MONGO":
            const result = await Records_Model.aggregate([
              { $match: queryModel.matchQuery() },
              {
                $project: {
                  _id: 0,
                  key: "$key",
                  createdAt: "$createdAt",
                  totalCount: { $sum: "$counts" },
                },
              },
              { $match: queryModel.countMatchQuery() },
              { $sort: { totalCount: 1 } },
            ])
              //.explain()
              .allowDiskUse(true)
              .exec();
            if (result.length) {
              return resolve(
                result.map((eachData: OutputModel) => {
                  return new OutputModel(
                    eachData.key,
                    eachData.createdAt,
                    eachData.totalCount
                  );
                })
              );
            } else {
              return resolve([]);
            }
          default:
            return reject(
              new CustomError(HttpStatusCode.ValidationError, "DB not set")
            );
        }
      } catch (err) {
        reject(err);
      }
    });
  }
  public seederProgram() {
    for (let j = 0; j < 10000; j++) {
      let date = this.randomDate(new Date(2001, 0, 1), new Date());
      let loop = Math.floor(Math.random() * 5);
      let key = this.randomKey();
      console.log("Upper Loop:", j, "Per loop:", loop, "Key:", key);
      for (let i = 0; i < loop; i++) {
        let count = Math.floor(Math.random() * 1000);
        new Records_Model({
          key: key,
          createdAt: date,
          count: count,
        }).save((err: any) => {
          if (err) {
            console.error(err);
          }
          console.log(key, "=>", count);
        });
      }
    }
  }
  private randomDate(start: Date, end: Date) {
    return new Date(
      start.getTime() + Math.random() * (end.getTime() - start.getTime())
    );
  }
  private randomKey(length: number = 16) {
    var result = [];
    var characters =
      "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
    var charactersLength = characters.length;
    for (var i = 0; i < length; i++) {
      result.push(
        characters.charAt(Math.floor(Math.random() * charactersLength))
      );
    }
    return result.join("");
  }
}
//new RecordRepo().seederProgram();
