import { RecordRepo } from "./../repositories/records.repository";
import { Container } from "typedi";
import { BaseService } from "./base.service";
import { InputModel, OutputModel } from "./../models";
const recordRepo = Container.get(RecordRepo);
export class RecordService extends BaseService {
  public getKeyCounts(
    startDate: string,
    endDate: string,
    minCount: number,
    maxCount: number
  ) {
    return new Promise(
      async (resolve, reject): Promise<OutputModel[] | any> => {
        try {
          resolve(
            await recordRepo.getKeyCounts(
              new InputModel(startDate, endDate, minCount, maxCount)
            )
          );
        } catch (err) {
          reject(err);
        }
      }
    );
  }
}
