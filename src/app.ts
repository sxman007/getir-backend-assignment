import "reflect-metadata"; // this shim is required
import { Container } from "typedi";
import { createExpressServer, useExpressServer } from "routing-controllers";
import { useContainer as routingUseContainer } from "routing-controllers";
import * as path from "path";
import * as hbs from "hbs";
// tslint:disable-next-line:no-var-requires
const express = require("express");
// set default app environment to development
process.env.NODE_ENV = process.env.NODE_ENV || "development";
routingUseContainer(Container);
// creates express app, registers all controller routes and returns you express app instance
export class App {
  public getApp() {
    const app = express();
    app.set("views", path.join(__dirname, "views"));
    app.set("view engine", "hbs");
    app.use(express.static(path.join(__dirname, "public")));
    useExpressServer(app, {
      controllers: [__dirname + "/controllers/*.ts"],
      middlewares: [__dirname + "/middlewares/*.ts"],
      cors: {
        origin: "*",
        methods: "GET,HEAD,PUT,PATCH,POST,DELETE",
        preflightContinue: false,
        optionsSuccessStatus: 204,
      },
      defaultErrorHandler: false,
    });
    
    return app;
  }
}
