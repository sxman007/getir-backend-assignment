import { OK } from "http-status-codes";
import { IResponseBody } from "../interfaces";

export class ResponseBuilder {
  public composeBody(attrs: {
    code: number;
    msg: string;
    records: Array<any>;
  }): IResponseBody {
    // default attributes of response body
    const bodyBase: any = {
      code: 0,
      msg: "Success",
    };

    // add additional attributes as well as overwrite existing ones with given values
    return bodyBase;
  }
}
