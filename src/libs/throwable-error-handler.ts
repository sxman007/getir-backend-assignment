import { HttpError } from 'routing-controllers';

export class ThrowableErrorHandler extends HttpError {
  public httpCode: number;
  public message: string;
  public dataObject: any;

  constructor(httpCode: number, message: string, data?: any) {
    super(httpCode, message);
    this.name = 'CustomError';
    this.httpCode = httpCode;
    this.message = message;
    this.dataObject = data;
  }
}
