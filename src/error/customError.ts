import { HttpError } from 'routing-controllers';
export class CustomError extends HttpError {
    message:string;
    _status_code : any;
    _status_type!: string;
    _status!: string;
    result:any;
    constructor(httpcode:number,message:string, extra:any = null) {
        super(httpcode,message);
        Error.captureStackTrace(this, this.constructor);
        this._status_code=httpcode;
        this.message = message;    
        this.result=extra;
    }
}
