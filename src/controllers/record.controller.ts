import { RecordService } from "./../services/record.service";
import { envConfigs } from "../configs/envs/index";
import { Container } from "typedi";
import { CustomError } from "../error/customError";
import {
  JsonController,
  UseBefore,
  Post,
  Req,
  Get,
  Param,
} from "routing-controllers";
import { Utility } from "@soham/utility";
import { AuthenticationMiddleware } from "../middlewares";
import { RequestCustom } from "src/interfaces/RequestCustom";
const recordService = Container.get(RecordService);
@JsonController(`${envConfigs.API_CONFIG.URL_PRIFIX}/record`)
export class MetricController {
  @UseBefore(AuthenticationMiddleware)
  @Post("/get")
  public async getKeyCountSum(@Req() request: RequestCustom) {
    return Utility.output(
      "Success",
      0,
      await recordService.getKeyCounts(
        request.body.startDate,
        request.body.endDate,
        request.body.minCount,
        request.body.maxCount
      )
    );
  }
}
