import { AppBase } from '../base';
import {
    Controller,
    Get,
    Render
  } from "routing-controllers";
/**
 *
 * @class BaseController
 */
@Controller("/")
export class BaseController extends AppBase {
    @Get("/")
    @Render("home.hbs")
    public async homePage() {
        return {
            server_name:this.envConfigs.SERVER.NAME,
            env:this.envConfigs.SERVER.ENV,
            company_short_name:this.envConfigs.COMPANY.SHORT_NAME,
            company_name:this.envConfigs.COMPANY.NAME,
            company_url:this.envConfigs.COMPANY.URL,
        };
    }
}
