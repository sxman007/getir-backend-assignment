import {Request} from 'express';
export interface RequestCustom extends Request
{
    headers:{
        Authorization:string,
    },
    query:any
}