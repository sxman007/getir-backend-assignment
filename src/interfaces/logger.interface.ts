import { Logger } from '@overnightjs/logger';

export interface ILogger {
  logger: Logger;
}
