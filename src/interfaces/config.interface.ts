import { IObjectGeneric } from './object.interface';

export interface IConfig {
  envConfigs: IObjectGeneric;
  appConfigs: IObjectGeneric;
}
