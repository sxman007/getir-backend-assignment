export interface IResponseBody {
  code: number;
  msg: string;
  records: Array<any>;
}
