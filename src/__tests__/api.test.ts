// tslint:disable-next-line:no-var-requires
const request = require("supertest");
import { Mongo } from "@soham/mongo";
import { App } from "../app";
import { Application } from "express";
import { envConfigs } from "../configs";
const app: Application = new App().getApp();
let server = app.listen(envConfigs.SERVER.PORT);
const validInput = {
  startDate: "2001-01-26",
  endDate: "2018-02-02",
  minCount: 2700,
  maxCount: 3000,
};

const invalidDataTypeInput = {
  startDate: "abc",
  endDate: "2018-02-02",
  minCount: "foobar",
  maxCount: 3000,
};

const missingInput = {
  endDate: "2018-02-02",
  minCount: 2700,
  maxCount: 3000,
};
describe("get search results", () => {
  it("should get error on invalid data type input[API][POST]", async () => {
    const res = await request(app)
      .post("/api/record/get")
      .send(invalidDataTypeInput);
    expect(res.body.code).toEqual(422);
    expect(res.body).toHaveProperty("records");
    expect(res.body.records).toEqual([]);
    expect(res.body).toHaveProperty("msg");
  });
  it("should get error on missing data input[API][POST]", async () => {
    const res = await request(app).post("/api/record/get").send(missingInput);
    expect(res.body.code).toEqual(422);
    expect(res.body).toHaveProperty("records");
    expect(res.body.records).toEqual([]);
    expect(res.body).toHaveProperty("msg");
  });
  it("should get result on valid data input[API][POST]", async () => {
    const res = await request(app).post("/api/record/get").send(validInput);
    expect(res.body.code).toEqual(0);
    expect(res.body).toHaveProperty("records");
    expect(res.body).toHaveProperty("msg");
    expect(res.body.msg).toEqual("Success");
    if (res.body.records.length) {
      expect(res.body.records).toContainEqual(
        expect.objectContaining({
          createdAt: expect.any(String),
          key: expect.any(String),
          totalCount: expect.any(Number),
        })
      );
    } else {
      expect(res.body.records).toEqual([]);
    }
  });
  beforeAll((done) => {
    done();
  });

  afterAll((done) => {
    // Closing the DB connection allows Jest to exit successfully.
    console.log("After all executed");
    server.close();
    Mongo.closeAll();
    done();
  });
});
