export const messageConfigs = {
    INVALID_ROUTE: 'Invalid route',
    GENERIC_ERROR: 'Something went wrong',
};
