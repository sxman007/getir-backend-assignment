process.env.NODE_ENV = process.env.NODE_ENV?process.env.NODE_ENV.trim() : "";
import { IServer } from "./interfaces/server.interface";
import { IStartServer } from "./interfaces/startserver.interface";
let orig = console.log;
process.env.NODE_ENV = process.env.NODE_ENV || "development";
import { CustomLoggerTool as Logger } from "./common/Logger";
import { envConfigs } from "./configs";
import { Mongo } from "@soham/mongo";
import { Application } from "express";
import { App } from "./app";
import * as path from "path";
import * as fs from "fs";
Logger.Info(`App running on env: ${process.env.NODE_ENV}`);
Logger.Warn(`Trying to connect to DB..: ${process.env.NODE_ENV}`);
let serverStarted = false;
const dbConnection = envConfigs.DB.CONNECTION;
bootstrap().catch((error) => console.error(error.stack));
function initializeDBStatus(dbConfig: any) {
  let { mongo = [], yogabyte = [], mssql = [] } = dbConfig;
  let dbStatus: any = {};
  switch (dbConnection.toString().toUpperCase()) {
    case "MONGO":
      for (let index in mongo) {
        dbStatus[mongo[<any>index]["database"]] = false;
      }
      break;
    default:
      break;
  }
  return dbStatus;
}

async function startServer(options: IStartServer | any = {}) {
  const { status, settings } = options;
  let server = null;
  let startServer = true;
  for (let index in settings) {
    let eachDB = settings[index];
    if (eachDB.database !== undefined) {
      if (status[eachDB.database] === undefined || !status[eachDB.database]) {
        startServer = false;
      }
    } else {
    }
  }
  if (startServer) {
    const app: Application = new App().getApp();
    server = app.listen(envConfigs.SERVER.PORT, (): void => {
      Logger.Info(`Server started on port ${envConfigs.SERVER.PORT}.`);
    });
    serverStarted = true;
  }
  return server;
}
function stopServer(options: IServer) {
  const { server } = options;
  if (server && serverStarted) {
    server.close();
    serverStarted = false;
  }
  return null;
}
async function bootstrap() {
  let server: any = null;
  let connectedDBStatus: any;
  try {
    switch (dbConnection.toString().toUpperCase()) {
      case "MONGO":
        const mongoDBSettings: any = envConfigs.DB.CONFIGS.MONGO;
        connectedDBStatus = initializeDBStatus({
          mongo: mongoDBSettings,
        });
        Mongo.getInstance(mongoDBSettings);
        const GetircaseStudy = Mongo.getConnection(mongoDBSettings[0].name);
        GetircaseStudy.on("connected", async () => {
          Logger.Info(
            `${mongoDBSettings[0].name} server: ${mongoDBSettings[0].server}`
          );
          Logger.Info(`${mongoDBSettings[0].name} DB connected.`);
          connectedDBStatus[mongoDBSettings[0].name] = true;
          server = await startServer({
            status: connectedDBStatus,
            settings: mongoDBSettings,
          });
        });
        GetircaseStudy.on("connecting", () => {
          Logger.Info(
            `trying to establish a connection to ${mongoDBSettings[0].name} DB`
          );
        });
        GetircaseStudy.on("reconnected", () => {
          Logger.Info(
            `${mongoDBSettings[0].name} DB reconnected:`,
            new Date().getTime()
          );
        });
        GetircaseStudy.on("reconnectTries", () => {
          Logger.Info(`Trying to reconnect ${mongoDBSettings[0].name} DB..`);
        });
        GetircaseStudy.on("error", (err: any) => {
          Logger.Info(
            `connection to ${mongoDBSettings[0].name} DB failed :  ${err}`
          );
        });
        GetircaseStudy.on("disconnected", () => {
          console.error(
            `${mongoDBSettings[0].name} DB connection closed on: `,
            new Date().getTime()
          );
          console.error(
            `Sorry!! Server has been stopped due to ${mongoDBSettings[0].name} DB connection closed..`
          );
          Logger.Warn(`Trying to connect to DB ${mongoDBSettings[0].name}`);
          connectedDBStatus[mongoDBSettings[0].name] = false;
          server = stopServer({ server: server });
        });
        break;
      default:
        server = await startServer();
        break;
    }
  } catch (err) {
    console.error(err);
  }
}
function exitHandler(options: any, exitCode: any) {
  if (options.cleanup) console.log("CleanUp", "clean");
  if (exitCode || exitCode === 0) console.log("Exit Code", exitCode);
  switch (dbConnection.toString().toUpperCase()) {
    case "MONGO":
      if (serverStarted) {
        Mongo.closeAll();
      }
      break;
  }
  process.exit(1);
}
process
  .on("unhandledRejection", (reason: any, p) => {
    switch (reason.name || "") {
      case "MongooseTimeoutError":
        break;
      default:
        console.error(reason);
        console.error(p);
        break;
    }
  })
  .on("uncaughtException", (err) => {
    console.error("##### Error in process! #####");
    console.error(err);
  })
  .on("exit", exitHandler.bind(null, { cleanup: true }))
  .on("SIGINT", exitHandler.bind(null, { exit: true }))
  .on("SIGUSR1", exitHandler.bind(null, { exit: true }))
  .on("SIGUSR2", exitHandler.bind(null, { exit: true }))
  .on("uncaughtException", exitHandler.bind(null, { exit: true }));
