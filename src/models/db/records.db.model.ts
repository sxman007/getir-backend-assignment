import { envConfigs } from "./../../configs";
import * as mongoose from "mongoose";
import { Mongo } from "@soham/mongo";
const database = envConfigs.DB.CONFIGS.MONGO[0].name;
let conn: any;
switch (`${envConfigs.DB.CONNECTION}_${envConfigs.env}`) {
  case "MONGO_DEVELOPMENT": {
    mongoose.set("debug", true);
    Mongo.getInstance(envConfigs.DB.CONFIGS.MONGO);
    conn = Mongo.getConnection(database);
    break;
  }
  case "MONGO_TEST":
    //mongoose.set("debug", true);
    Mongo.getInstance(envConfigs.DB.CONFIGS.MONGO);
    conn = Mongo.getConnection(database);
    break;
  case "MONGO_PRODUCTION":
    conn = Mongo.getConnection(database);
    break;
}

export type RecordsDataDocument = mongoose.Document & {
  key: string;
  counts: Array<number>;
  createdAt: string;
};

const recordsDataSchema = new mongoose.Schema<RecordsDataDocument>(
  {
    key: { type: String, require: true, index: true },
    counts: { type: Array },
    createdAt: { type: Date, default: new Date() },
  },
  { versionKey: false }
);

export const Records_Model =
  envConfigs.DB.CONNECTION === "MONGO"
    ? conn.model("records", recordsDataSchema, "records")
    : null;
recordsDataSchema.index({ counts: 1 });
recordsDataSchema.index({ createdAt: 1 });
