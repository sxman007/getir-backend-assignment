import { AppBase } from '../base';
import * as mongoose from 'mongoose';
/**
 *
 * @class BaseModel
 */
export class BaseModel extends AppBase {
    static convertToObjectID(string:string){
        return mongoose.Types.ObjectId(string);
    }
    static checkObjectId =  (id:string) => {
        var checkForHexRegExp = /^(?=[a-f\d]{24}$)(\d+[a-f]|[a-f]+\d)/i;
        if (id === "undefined") {
            return false;
        }
        return checkForHexRegExp.test(id);
    }
}
