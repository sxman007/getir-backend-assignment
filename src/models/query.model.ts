import { InputModel } from "./input.model";
import { HttpStatusCode } from "@soham/config";
import { CustomError } from "../error/customError";
export class QueryModel {
  public query: any;
  private inputModel: InputModel;
  constructor(inputModel: InputModel) {
    this.inputModel = inputModel;
  }
  public matchQuery() {
    return {
      createdAt: {
        $gte: new Date(this.inputModel.startDate),
        $lte: new Date(this.inputModel.endDate),
      },
    };
  }
  public countMatchQuery() {
    return {
      totalCount: {
        $gte: this.inputModel.minCount,
        $lte: this.inputModel.maxCount,
      },
    };
  }
  public project(){
    return {
      _id: 0,
      key: "$key",
      createdAt: "$createdAt",
      totalCount: "$totalCount",
    };
  }
}
