import { HttpStatusCode } from "@soham/config";
import { CustomError } from "../error/customError";
export class InputModel {
  public startDate!: string;
  public endDate!: string;
  public minCount!: number;
  public maxCount!: number;
  constructor(
    startDate: string,
    endDate: string,
    minCount: number,
    maxCount: number
  ) {
    this.startDate = startDate;
    this.endDate = endDate;
    this.minCount = minCount;
    this.maxCount = maxCount;
    this.validate();
  }
  private validate() {
    if (isNaN(new Date(this.startDate).getTime())) {
      throw new CustomError(
        HttpStatusCode.ValidationError,
        "Invalid startDate"
      );
    }
    if (isNaN(new Date(this.endDate).getTime())) {
      throw new CustomError(HttpStatusCode.ValidationError, "Invalid endDate");
    }
    if (isNaN(this.minCount) || this.minCount < 0) {
      throw new CustomError(HttpStatusCode.ValidationError, "Invalid mincount");
    }
    if (isNaN(this.maxCount) || this.maxCount < 0) {
      throw new CustomError(HttpStatusCode.ValidationError, "Invalid maxCount");
    }
    this.minCount = Number(this.minCount);
    this.maxCount = Number(this.maxCount);
    if (this.minCount>this.maxCount) {
      throw new CustomError(HttpStatusCode.ValidationError, "maxxCount value must be greater than or equal to minCount");
    }
  }
}
