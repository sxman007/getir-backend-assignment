import { HttpStatusCode } from "@soham/config";
import { CustomError } from "../error/customError";
export class OutputModel {
  public key!: string;
  public createdAt!: string;
  public totalCount!: number;
  constructor(key: string, createdAt: string, totalCount: number) {
    this.key = key;
    this.createdAt = createdAt;
    this.totalCount = totalCount;
    this.validate();
  }
  private validate() {
    if (!this.key) {
      throw new CustomError(HttpStatusCode.ValidationError, "Key not found");
    }
    if (isNaN(new Date(this.createdAt).getTime())) {
      console.error("Received createdAt:", this.createdAt);
      throw new CustomError(
        HttpStatusCode.ValidationError,
        "Invalid createdAt"
      );
    }
    if (isNaN(this.totalCount) || this.totalCount < 0) {
      console.error("Received totalCount:", this.totalCount);
      throw new CustomError(
        HttpStatusCode.ValidationError,
        "Invalid totalCount"
      );
    }
  }
}
