import { ExpressMiddlewareInterface } from "routing-controllers";
import { CustomError } from '../../error';
import { HttpStatusCode } from '@soham/config';
import Container from 'typedi';
export class AuthenticationMiddleware implements ExpressMiddlewareInterface {
  public async use(req: any, res: any, next:any) {
    next();
  }
}
