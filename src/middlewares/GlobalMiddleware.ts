import { ExpressMiddlewareInterface, Middleware } from "routing-controllers";
import { json, urlencoded } from "body-parser";
@Middleware({ type: "before" })
export class HeaderCorrection implements ExpressMiddlewareInterface {
  use(request: any, response: any, next: any): any {
    next();
  }
}

@Middleware({ type: "before" })
export class JsonBodyParserMiddleware implements ExpressMiddlewareInterface {
  use(request: any, response: any, next: any): any {
    return json({ limit: "50mb" })(request, response, next);
  }
}

@Middleware({ type: "before" })
export class UrlencodedBodyParserMiddleware
  implements ExpressMiddlewareInterface {
  use(request: any, response: any, next: any): any {
    return urlencoded({ limit: "50mb", extended: true })(
      request,
      response,
      next
    );
  }
}
