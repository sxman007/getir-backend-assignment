// tslint:disable: max-classes-per-file

import {
  ExpressMiddlewareInterface,
  ExpressErrorMiddlewareInterface,
  Middleware,
  NotFoundError
} from "routing-controllers";
import { Request, Response, NextFunction } from "express";
import { INTERNAL_SERVER_ERROR } from "http-status-codes";
import { IResponseBody } from "../interfaces";
import { AppBase } from "../base";
import { ThrowableErrorHandler } from "../libs";
import { messageConfigs } from "../configs";

@Middleware({ type: "after" })
export class UnmatchedRouteErrorHandler extends AppBase
  implements ExpressMiddlewareInterface {
  public use(req: Request, res: Response, next: NextFunction): any {
    if (!res.headersSent) {
      throw new NotFoundError(
        `${messageConfigs.INVALID_ROUTE} ${req.originalUrl}`
      );
    }
    res.end();
  }
}

@Middleware({ type: "after" })
export class GlobalErrorHandler extends AppBase
  implements ExpressErrorMiddlewareInterface {
  public error(
    error: ThrowableErrorHandler,
    request: Request,
    response: Response,
    next: NextFunction
  ): void {
    if(error.httpCode===404){
      this.logger.err(`Invalid Route : ${error.message} [${request.method}]`);
    }else{
      this.logger.err(`Error occured : ${error.message}`);
      if(error.stack!==undefined){
        this.logger.err(`Error occured : ${error.stack}`);
      }
    }
    let statusCodeString = "";
    const statusCode = error.httpCode || INTERNAL_SERVER_ERROR;
    switch (statusCode) {
      case 204:
        statusCodeString = "VALIDATION_ERROR";
        break;
      case 422:
        statusCodeString = "VALIDATION_ERROR";
        break;
      case 419:
        statusCodeString = "INVALID_KEY";
        break;
      case 499:
        statusCodeString = "TOKEN_REQUIRED";
        break;
      case 498:
        statusCodeString = "INVALID_AUTH_TOKEN";
        break;
      case 406:
        statusCodeString = "NOT_ACCEPTABLE";
        break;
      case 404:
        statusCodeString = "NOT_FOUND";
        break;
      case 500:
        statusCodeString = "INTERNAL_SERVER_ERROR";
        break;
      case 203:
        statusCodeString = "NON_AUTHORITATIVE_INFORMATION";
        break;
    }
    const responseData: IResponseBody = {
      msg: error.message || messageConfigs.GENERIC_ERROR,
      code: statusCode,
      records:[]
    };
    response.status(statusCode).json(responseData);
  }
}
