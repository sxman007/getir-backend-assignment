# Getir backend Assignment [Typescript + NodeJS]

This is a backend assignment provided by Getir technical round interview.

## Problem Statement

RESTful API with a single endpoint that fetches the data in the
provided MongoDB collection and return the results in the requested format.

## Configuration

You will find the configuration files under _/src/configs/envs/data_/
Two files is here

1. developement.json [For Development environment]
2. test.json [For Test environment]

You may change the configuration value as per your need.

## Data Source

You will find the configuration files under _/src/configs/envs/data_/

Under

```javascript
DB->CONNECTION [Accepted values "MONGO"]
```

You may change the configuration value as per your need.

## Installation

Install global npm packages

```js
sudo npm install -g typescript@3.8.3 ts-node@8.6.2
```

Install project npm packages

```js
npm install && cp -R custom_node_modules/* node_modules/
```

## Run

For run the application in development environment

_Windows_

```js
set NODE_ENV=development && npm start
```

_Linux_

```js
export NODE_ENV=development && npm start
```

For run the application in production environment using pm2

```js
sudo npm install -g pm2

pm2 start pm2-node-app.json

```

The application server will be start on environment configuration mentioned port [Default=8017].

## Run Test Cases

Two test files are here, under _/src/*_tests_*

1. api.test.ts

_Windows_

```js
npm run test

or

npx jest --silent
```

## API Documentation

### 1. Get sum of counts of key records

```http
POST /api/record/get
```

#### Query Params

| Parameter | Type      | Description |
| :-------- | :-------- | :---------- |
| `Nothing` | `Nothing` | `Nothing`   |

#### Headers

| Parameter      | Type               | Description  |
| :------------- | :----------------- | :----------- |
| `Content-Type` | `application/json` | **Required** |

#### Body

```javascript
{
"startDate": "2001-01-26",
"endDate": "2018-02-02",
"minCount": 2700,
"maxCount": 3000
}
```

#### Response

```javascript
{
    "code": 0,
    "msg": "Success",
    "records": [
        {
            "key": "OT6b1r4557X2CVtb",
            "createdAt": "2003-07-15T03:03:58.432Z",
            "totalCount": 2704
        },
        {
            "key": "CzNh4IEaTVhmjwWV",
            "createdAt": "2015-05-09T03:38:23.527Z",
            "totalCount": 2707
        }
    ]
}
```

